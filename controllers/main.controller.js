class MainController {
    static async getAllPokemon(user_id) {
        try {
            const res = await fetch(`http://localhost:3000/api/${user_id}/pokemon`);
            return res.json();
        } catch (error) {
            return {
                error: 'Something went wrong fetching pokemon info'
            }
        }
    }

    static async getAllShinies(user_id) {

        const res = await fetch(`http://localhost:3000/api/${user_id}/shinies`);
        return await res.json();
    }

    static async getUserInfo(user_id) {
        try {
            const res = await fetch(`http://localhost:3000/api/${user_id}/user`);
            return res.json();

        } catch (error) {
            return {
                error: 'Something went wrong fetching user info'
            }
        }
    }
}

export default MainController;