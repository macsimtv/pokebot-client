/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    DISCORD_TOKEN: process.env.DISCORD_TOKEN,
  }
}

module.exports = nextConfig
