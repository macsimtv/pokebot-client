import prisma from "prisma/server";

const token = process.env.DISCORD_TOKEN;

export default async function handler(req, res) {
  try {
    const userDB = await prisma.users.findUnique({
      where: { id: req.query.user_id },
    });

    if (!userDB) {
      return res.status(404).json({
        error: "No user found",
      });
    }

    const headers = {
      Authorization: `Bot ${token}`,
    };

    const response = await fetch(
      `https://discord.com/api/v9/users/${userDB.discordID}`,
      {
        headers: headers,
      }
    );

    const user = await response.json();

    if (user) {
      return res.status(200).json(user);
    }

    res.status(404).json({
      error: "No user found",
    });
  } catch (error) {
    res.status(500).json({
      error: "Something went wrong fetching user info in api",
    });
  }
}
