import prisma from "prisma/server";

export default async function handler(req, res) {
  try {
    const pokemons = await prisma.pokemons.findMany({ where: { user: req.query.user_id }, orderBy: [{ name: 'asc' }] });

    if(pokemons.length === 0) {
      return res.status(404).json({
        error: "No pokemon found"
      });
    }
  
    res.status(200).json(pokemons);
  } catch (error) {
    res.status(500).json({
      error: 'Something went wrong'
    });
  }
}