import Image from 'next/image';

import MainController from 'controllers/main.controller';

import pokemonNameBeautify from 'lib/pokemonNameBeautify';

export default function UserPokemon({ userInfo, pokemons }) {

  return (
    <>
      <div className='user_id-pokemon-page'>
        <section className='user-info'>
          <div className="user-info__container container">
            <div className="user-info__avatar">
              <img src={userInfo.avatar} alt={userInfo.name} />
            </div>
            <div className="user-info__name">
              <h2>{userInfo.username}&apos;s pokedex</h2>
              <p>
                {userInfo.id}
              </p>
            </div>
          </div>
        </section>
        <section className='user-pokemons'>
          <div className="user-pokemons__container container">
            <div className="user-pokemons__search">
              <input type="text" placeholder="Search for a pokemon" />
            </div>
            <div className="user-pokemons__list">
              {pokemons.map(pokemon => (
                <div data-shiny={pokemon.shiny ? '✨' : ''} className="user-pokemons__item" key={pokemon.id}>
                  <div className="user-pokemons__item-name">
                    <h3>{pokemonNameBeautify(pokemon.name)}</h3>
                    <p>
                      {pokemon.quantity}x
                    </p>
                  </div>
                  <div className="user-pokemons__item-avatar">
                    <img src={`https://play.pokemonshowdown.com/sprites/xyani${pokemon.shiny ? '-shiny' : ''}/${pokemon.name}.gif`} alt={pokemon.name} />
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
      </div>
    </>
  )
}

export const getServerSideProps = async ({ query }) => {
    const pokemons = await MainController.getAllPokemon(query.user_id);
    const userInfo = await MainController.getUserInfo(query.user_id);

    return {
        props: {
            pokemons,
            userInfo: {
              id: userInfo.id,
              username: userInfo.username,
              avatar: `https://cdn.discordapp.com/avatars/${userInfo.id}/${userInfo.avatar}.png`
            }
        }
    }
}
